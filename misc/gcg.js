
var Gcg = Gcg || {};
Gcg.geocoder = null;
Gcg.resultFields = [];

/**
 * Attaches the autocomplete behaviour to all required fields
 */
Gcg.geocoderAutoAttach = function () {
  $('input.gcg-query').each(function () {
    if (!Gcg.geocoder) {
      if (GBrowserIsCompatible()) {
        Gcg.geocoder = new GClientGeocoder();
        Gcg.resultFields = ['address', 'latitude', 'longitude', 'adminarea', 'subadminarea', 'locality', 'deplocality', 'thoroughfare', 'postalcode'];
      } else {
        alert('Unsupported browser');
        return;
      }
    }
    var base = '#' + this.id.substr(0, this.id.length - 6);
    var custom = $(base + '-custom').get(0).value;
    if (custom == 0 && Drupal.settings.gcg[base + '-settings'].disableResult) {
      $(base + '-accuracy').attr('disabled', true);
      $(base + '-country').attr('disabled', true);
      for (i in Gcg.resultFields) {
        $(base + '-' + Gcg.resultFields[i]).attr('disabled', true);
      }
      Gcg.toggleResults(base);
    }
    $(":submit", this.form).click( function() {
      $(base + '-accuracy').removeAttr('disabled');
      $(base + '-country').removeAttr('disabled');
      for (i in Gcg.resultFields) {
        $(base + '-' + Gcg.resultFields[i]).removeAttr('disabled');
      }
    });
    $(this.form).submit(Gcg.geocoderFormSubmit(base));
    new Gcg.jsGC(this, base);
  });
}

Gcg.geocoderFormSubmit = function () {
  return $('#gcg-search-result').each(function () {
    this.owner.hidePopup();
  }).size() == 0;
}

Gcg.toggleResults = function(base) {
  var custom = $(base + '-custom').get(0).value;
  if (custom == 0 && Drupal.settings.gcg[base + '-settings'].disableResult) {
    var accuracy = $(base + '-accuracy').get(0);
    var country = $(base + '-country').get(0);
    if (typeof(Gcg.invalidResult) == 'undefined' || (!Gcg.invalidResult && (!Drupal.settings.gcg[base + '-settings'].enableCustomResult || accuracy.value != 0))) {
      $(accuracy).attr('disabled', true);
      $(country).attr('disabled', true);
    } else {
      $(accuracy).removeAttr('disabled');
      $(country).removeAttr('disabled');
    }
    for (i in Gcg.resultFields) {
      $(base + '-' + Gcg.resultFields[i])
        .each(function() {
          if (typeof(Gcg.invalidResult) == 'undefined' || (!Gcg.invalidResult && (!Drupal.settings.gcg[base + '-settings'].enableCustomResult || accuracy.value != 0))) {
            $(this).attr('disabled', true);
          } else {
            $(this).removeAttr('disabled');
          }
        });
    }
  }
}

Gcg.parseResponse = function (response) {
  var gc = Gcg.popupOwner;
  if (!response || response.Status.code != 200) {
    var pm = {};
    pm.address = Drupal.settings.gcg.invalidAddress;
    pm.AddressDetails = {};
    pm.AddressDetails.Accuracy = 0;
    Gcg.extractResponse(gc.base, pm, gc.map);
  } else {
    if (response.Placemark.length == 1) {
      Gcg.extractResponse(gc.base, response.Placemark[0], gc.map);
    } else {
      var ul = document.createElement('ul');
      for (i in response.Placemark) {
        var place = response.Placemark[i];
        var li = document.createElement('li');
        $(li)
          .html('<div>'+ Drupal.settings.gcg.resultLevels[place.AddressDetails.Accuracy] + ' - ' + place.address +'</div>')
          .mousedown(function () { gc.select(this); gc.hidePopup(); })
          .mouseover(function () { gc.highlight(this); })
          .mouseout(function () { gc.unhighlight(this); });
        li.gcgSearchResultValue = place;
        $(ul).append(li);
      }
      if (gc.popup) {
        if (ul.childNodes.length > 0) {
          $(gc.popup).empty().append(ul).show();
        }
        else {
          $(gc.popup).css({visibility: 'hidden'});
          gc.hidePopup();
        }
      }
    }
  }
}

Gcg.extractResponse = function (base, placemark, map) {
  Gcg.clearResponse(base, map);
  if (placemark.AddressDetails.Accuracy == 0) {
    $(base + '-accuracy').get(0).value = placemark.AddressDetails.Accuracy;
    $(base + '-address').get(0).value = placemark.address;
    $(base + '-country').get(0).value = '--';
    $(base + '-custom').get(0).value = 1;
    Gcg.invalidResult = true;
    Gcg.toggleResults(base);
    return;
  }

  $(base + '-accuracy').get(0).value = placemark.AddressDetails.Accuracy;
  $(base + '-custom').get(0).value = 0;

  Gcg.addMarker(base, map, placemark.Point.coordinates[1], placemark.Point.coordinates[0], placemark.address, placemark.AddressDetails.Accuracy + 3);
  //Debug:
  //$(base + '-result-result').get(0).value = placemark.toSource();

  Gcg.invalidResult = false;
  Gcg.toggleResults(base);

  $(base + '-address').get(0).value = placemark.address;
  $(base + '-latitude').get(0).value = placemark.Point.coordinates[1];
  $(base + '-longitude').get(0).value = placemark.Point.coordinates[0];
  $(base + '-country').get(0).value = placemark.AddressDetails.Country.CountryNameCode;
  var acc = placemark.AddressDetails.Accuracy;
  if (acc == 5) {
    $(base + '-postalcode').get(0).value = placemark.AddressDetails.Country.PostalCode.PostalCodeNumber;
  } else if (acc > 1) {
    $(base + '-adminarea').get(0).value = placemark.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName;
    if (acc > 2) {
      if (typeof(placemark.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea) != 'undefined') {
        var cont = placemark.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea;
        $(base + '-subadminarea').get(0).value = cont.SubAdministrativeAreaName;
      } else {
        var cont = placemark.AddressDetails.Country.AdministrativeArea;
      }
      if (acc > 3) {
        $(base + '-locality').get(0).value = cont.Locality.LocalityName;
        if (acc > 5) {
          if (typeof(cont.Locality.DependentLocality) != 'undefined') {
            var loccont = cont.Locality.DependentLocality;
            $(base + '-deplocality').get(0).value = loccont.DependentLocalityName;
          } else {
            var loccont = cont.Locality;
          }
          $(base + '-thoroughfare').get(0).value = loccont.Thoroughfare.ThoroughfareName;
          if (typeof(loccont.PostalCode) != 'undefined') {
            $(base + '-postalcode').get(0).value = loccont.PostalCode.PostalCodeNumber;
          }
        }
      }
    }
  }
}

Gcg.clearResponse = function (base, map) {
  if (typeof(map) != 'undefined' && map) {
    map.clearOverlays();
  }
  $(base + '-accuracy').get(0).value = 0;
  $(base + '-country').get(0).value = '--';
  $(base + '-custom').get(0).value = 0;
  for (i in Gcg.resultFields) {
    $(base + '-' + Gcg.resultFields[i]).get(0).value = '';
  }
  //$(base + '-result-result').get(0).value = '';
}

Gcg.resetResult = function (base, map, values) {
  Gcg.clearResponse(base, map);
  $(base + '-query').get(0).value = '';
  $(base + '-accuracy').get(0).value = values['accuracy'];
  $(base + '-country').get(0).value = values['country'];
  for (i in Gcg.resultFields) {
    $(base + '-' + Gcg.resultFields[i]).get(0).value = values[Gcg.resultFields[i]];
  }
  if (values['accuracy'] > 0 && values['latitude'] != '' && values['longitude'] != '') {
    Gcg.addMarker(base, map, values['latitude'], values['longitude'], values['address'], values['accuracy'] + 3);
  }
}

Gcg.addMarker = function(base, map, lat, lng, title, zoom) {
  if (typeof(map) != 'undefined' && map) {
    map.clearOverlays();
    var point = new GLatLng(lat, lng);
    var icon = new GIcon(G_DEFAULT_ICON);
    title = (title) ? title : '';
    markerOpts = {icon: icon, title: title, draggable: true};

    var marker = new GMarker(point, markerOpts)    

    GEvent.addListener(marker, "mousedown", function() { Gcg.toggleMarker(base, marker);
    });

    GEvent.addListener(marker, "dragend", function() {
      if (marker.draggingEnabled()) {
        var endpoint = marker.getLatLng();      
        $(base + '-latitude').get(0).value = endpoint.y;
        $(base + '-longitude').get(0).value = endpoint.x;
        map.setCenter(endpoint);
      } else {
        marker.setLatLng(point);
      }
    });

    map.setCenter(point, zoom);
    map.addOverlay(marker);
  }
}

Gcg.toggleMarker = function(base, marker) {
  var custom = $(base + '-custom').get(0).value;
  var accuracy = $(base + '-accuracy').get(0);
  if (typeof(Gcg.invalidResult) == 'undefined' ||
      (custom == 0 && Drupal.settings.gcg[base + '-settings'].disableResult &&
        !Gcg.invalidResult && (!Drupal.settings.gcg[base + '-settings'].enableCustomResult || accuracy.value != 0))) {
    marker.disableDragging();
  } else {
    marker.enableDragging();
  }
}

Gcg.jsGC = function (input, base) {
  var gc = this;
  this.input = input;
  this.base = base;
  this.defaultValues = [];

  this.defaultValues['accuracy'] = $(this.base + '-accuracy').get(0).value;
  this.defaultValues['country'] = $(this.base + '-country').get(0).value;
  this.defaultValues['custom'] = $(this.base + '-custom').get(0).value;
  for (i in Gcg.resultFields) {
    this.defaultValues[Gcg.resultFields[i]] = $(this.base + '-' + Gcg.resultFields[i]).get(0).value;
  }

  $(this.input)
    .keydown(function (event) { return gc.onkeydown(this, event); })
    .blur(function () { gc.hidePopup(); });

  $(base + '-search')
    .click(function () { gc.onclick() });

  $(base + '-edit')
    .click(function () { Gcg.invalidResult = true; $(gc.base + '-custom').get(0).value = 1; Gcg.toggleResults(gc.base); });

  $(base + '-reset')
    .click(function () { Gcg.invalidResult = undefined; Gcg.resetResult(gc.base, gc.map, gc.defaultValues); Gcg.toggleResults(gc.base); });

  $(base + '-map').each(function () {
    gc.map = new GMap2(document.getElementById(this.id));
    gc.map.setCenter(new GLatLng(47.498403, 19.040759), 10);
    gc.map.setMapType(G_NORMAL_MAP);
    gc.map.addControl(new GSmallZoomControl());
    gc.map.enableDragging();
    gc.map.disableDoubleClickZoom();
    gc.map.disableScrollWheelZoom();
    
    if (!Drupal.settings.gcg[base + '-settings'].disableResult || Drupal.settings.gcg[base + '-settings'].enableCustomResult) {
      GEvent.addListener(gc.map, "dblclick", function(overlay, point) {
        if ((typeof(Gcg.invalidResult) == 'undefined' && !Drupal.settings.gcg[base + '-settings'].disableResult) || (Gcg.invalidResult && Drupal.settings.gcg[base + '-settings'].enableCustomResult)) {
          $(gc.base + '-latitude').get(0).value = point.y;
          $(gc.base + '-longitude').get(0).value = point.x;
          var zoom = $(base + '-accuracy').get(0).value;
          zoom = (typeof(zoom) != 'undefined' && zoom != 0) ? zoom + 3 : 11;
          Gcg.addMarker(base, gc.map, point.y, point.x, $(base + '-address').get(0).value, zoom);
        }
      });
    }

    var lat = $(base + '-latitude').get(0).value;
    var lng = $(base + '-longitude').get(0).value;
    if (lat && lng) {
      var zoom = $(base + '-accuracy').get(0).value;
      zoom = (typeof(zoom) != 'undefined' && zoom != 0) ? new Number(zoom) + 3 : 11;
      Gcg.invalidResult = false;
      Gcg.addMarker(base, gc.map, lat, lng, $(base + '-address').get(0).value, zoom);
    }
  });
};

Gcg.jsGC.prototype.onkeydown = function (input, e) {
  if (!e) {
    e = window.event;
  }
  switch (e.keyCode) {
    case 27: // esc
      if (this.popup) {
        this.selected = false;
        this.hidePopup();
      }
      return true;
    case 40: // down arrow
      if (this.popup) {
        this.selectDown();
      }
      return true;
    case 38: // up arrow
      if (this.popup) {
        this.selectUp();
      }
      return true;
    case 13: // enter
      if (this.popup) {
        this.hidePopup(e.keyCode);
      } else {
        this.onclick();
      }
      return false;
    default: // all other keys
      return true;
  }
}

/*Gcg.jsGC.prototype.onkeyup = function (input, e) {
  if (!e) {
    e = window.event;
  }
  switch (e.keyCode) {
    case 13: // enter
      if (this.popup) {
        this.hidePopup(e.keyCode);
      } else {
        this.onclick();
      }
      return true;

    default: // all other keys
      return true;
  }
}*/

Gcg.jsGC.prototype.onclick = function () {
  if (this.input.value.length > 0) {
    this.populatePopup();
    Gcg.popupOwner = this;
    Gcg.geocoder.getLocations(this.input.value, Gcg.parseResponse);
  }
  return true;
}

Gcg.jsGC.prototype.select = function (node) {
  this.selected = node;
  //Gcg.extractResponse(this.base, node.gcgSearchResultValue, this.map);
}

Gcg.jsGC.prototype.selectDown = function () {
  if (this.selected && this.selected.nextSibling) {
    this.highlight(this.selected.nextSibling);
  }
  else {
    var lis = $('li', this.popup);
    if (lis.size() > 0) {
      this.highlight(lis.get(0));
    }
  }
}

Gcg.jsGC.prototype.selectUp = function () {
  if (this.selected && this.selected.previousSibling) {
    this.highlight(this.selected.previousSibling);
  }
}

Gcg.jsGC.prototype.highlight = function (node) {
  if (this.selected) {
    $(this.selected).removeClass('selected');
  }
  $(node).addClass('selected');
  this.selected = node;
}

Gcg.jsGC.prototype.unhighlight = function (node) {
  $(node).removeClass('selected');
  this.selected = false;
}

Gcg.jsGC.prototype.hidePopup = function (keycode) {
  // Select item if the right key or mousebutton was pressed
  if (this.selected && ((keycode && keycode != 46 && keycode != 8 && keycode != 27) || !keycode)) {
    Gcg.extractResponse(this.base, this.selected.gcgSearchResultValue, this.map);
  }
  // Hide popup
  var popup = this.popup;
  if (popup) {
    this.popup = null;
    $(popup).fadeOut('fast', function() { $(popup).remove(); });
  }
  this.selected = false;
}

Gcg.jsGC.prototype.populatePopup = function (results) {
  // Show popup
  if (this.popup) {
    $(this.popup).remove();
  }
  this.selected = false;
  this.popup = document.createElement('div');
  this.popup.id = 'gcg-search-result';
  this.popup.owner = this;
  $(this.popup).css({
    marginTop: this.input.offsetHeight +'px',
    width: (this.input.offsetWidth - 4) +'px',
    display: 'none'
  });
  $(this.input).before(this.popup);
}

// Global Killswitch
if (Drupal.jsEnabled) {
  $(document).ready(Gcg.geocoderAutoAttach);
}
