
var GcgNode = GcgNode || {};

/**
 * Attaches the autocomplete behaviour to all required fields
 */
GcgNode.loadMap = function () {
  if (GBrowserIsCompatible()) {
    var point = new GLatLng(GcgNode.conf.address.latitude, GcgNode.conf.address.longitude);
    var zoom = 3;
    zoom = zoom + new Number(GcgNode.conf.address.accuracy);
    var mapOpts = {};
    if (GcgNode.conf.type == 'normal') {
      mapOpts.mapTypes = [G_NORMAL_MAP];
    } else if (GcgNode.conf.type == 'satellite') {
      mapOpts.mapTypes = [G_SATELLITE_MAP];
    } else if (GcgNode.conf.type == 'hybrid') {
      mapOpts.mapTypes = [G_HYBRID_MAP];
    } else if (GcgNode.conf.type == 'physical') {
      mapOpts.mapTypes = [G_PHYSICAL_MAP];
    }
    var map = new GMap2(document.getElementById('gcg-node-address-map'), mapOpts);
    map.setCenter(point, zoom, mapOpts.mapTypes[0]);
    if (GcgNode.conf.drag == 1) {
      map.enableDragging();
    } else {
      map.disableDragging();
    }

    if (GcgNode.conf.zoom == 1) {
      map.enableDoubleClickZoom();
    } else {
      map.disableDoubleClickZoom();
    }
    map.disableScrollWheelZoom();
    if (GcgNode.conf.drag == 1 && GcgNode.conf.zoom == 1 && GcgNode.conf.scontrol == 1) {
      map.addControl(new GSmallMapControl());
    } else if (GcgNode.conf.zoom == 1){
      map.addControl(new GSmallZoomControl());
    }

    var icon = new GIcon(G_DEFAULT_ICON);
    var title = (GcgNode.conf.address.privacy > 0) ? GcgNode.conf.protectedAddress : GcgNode.conf.address.address;
    markerOpts = {icon: icon, title: title};

    var marker = new GMarker(point, markerOpts)    

    map.addOverlay(marker);
  } else {
    alert('Unsupported browser');
    return false;
  }
}

GcgNode.loadStaticMap = function() {
  var cont = $('#gcg-node-address-map').get(0);
  var zoom = 3;
  zoom = zoom + new Number(GcgNode.conf.address.accuracy);
  var lat = GcgNode.conf.address.latitude;
  var lng = GcgNode.conf.address.longitude;
  var title = (GcgNode.conf.address.privacy > 0) ? GcgNode.conf.protectedAddress : GcgNode.conf.address.address;
  $(cont).html('<img alt="' + title + '" title="' + title + '" src="http://maps.google.com/staticmap?center=' + lat + ',' + lng + '&zoom=' + zoom + '&size=' + $(cont).width() + 'x' + $(cont).height() + '&maptype=' + GcgNode.conf.stype +
    '&markers=' + lat + ',' + lng + '&key=' + GcgNode.conf.apiKey + '" />');
}

// Global Killswitch
if (Drupal.jsEnabled) {
  $(document).ready( function () {
    GcgNode.conf = Drupal.settings.gcgNode;
    if (GcgNode.conf.height > 0) {
      $('#gcg-node-address-map').css('height', GcgNode.conf.height + 'px');
    }
    if (GcgNode.conf.onLoad == 1) {
      if (GcgNode.conf.apiType == 'dynamic') {
        GcgNode.loadMap();
      } else {
        GcgNode.loadStaticMap();
      }
    } else {
      $('a', $("#gcg-node-address-map")).click( function() {
        if (GcgNode.conf.apiType == 'dynamic') {
          GcgNode.loadMap();
        } else {
          GcgNode.loadStaticMap();
        }
        return false;
      });
    }
  });
}
