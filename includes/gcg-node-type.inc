<?php

function _gcg_node_type_form(&$form) {
  $enabled = variable_get('gcg_node_type_enabled_' . $form['#node_type']->type, 0);
  $form['#validate']['gcg_node_form_alter_validate'] = array();
  $form['gcg_node'] = array('#type' => 'fieldset',
    '#title' => t('GCG Node settings'),
    '#collapsible' => TRUE,
    '#collapsed' => !$enabled,
    '#access' => user_access('administer gcg node'),
  );
  $form['gcg_node']['gcg_node_type_enabled'] = array('#type' => 'checkbox',
    '#title' => t('Enable GCG addressing for this node type.'),
    '#default_value' => $enabled,
  );
  $form['gcg_node']['gcg_node_input'] = array('#type' => 'fieldset',
    '#title' => t('Input settings'),
  );
  $form['gcg_node']['gcg_node_input']['gcg_node_result_format'] = array('#type' => 'radios',
    '#title' => t('Result display format'),
    '#default_value' => variable_get('gcg_node_result_format_' . $form['#node_type']->type, 'system'),
    '#options' => array('system' => t('System default'), 'compact' => t('Compact - accuracy, address and coordinates'), 'full' => t('Full - all available fields')),
  );
  $form['gcg_node']['gcg_node_input']['gcg_node_result_on_map'] = array('#type' => 'radios',
    '#title' => t('Display result on map'),
    '#default_value' => variable_get('gcg_node_result_on_map_' . $form['#node_type']->type, -1),
    '#options' => array(-1 => t('System default'), 0 => t('Disabled'), 1 => t('Enabled')),
  );
  $form['gcg_node']['gcg_node_input']['gcg_node_result_map_height'] = array('#type' => 'textfield',
    '#title' => t('Height of the result map if displayed'),
    '#default_value' => variable_get('gcg_node_result_map_height_' . $form['#node_type']->type, -1),
    '#size' => 5,
    '#maxlength' => 3,
    '#field_suffix' => 'px',
    '#description' => t('-1 means system default.'),
  );
  $form['gcg_node']['gcg_node_input']['gcg_node_disabled'] = array('#type' => 'radios',
    '#title' => t('Disable result fields'),
    '#default_value' => variable_get('gcg_node_disabled_' . $form['#node_type']->type, -1),
    '#options' => array(-1 => t('System default'), 0 => t('No'), 1 => t('Yes')),
  );
  $form['gcg_node']['gcg_node_input']['gcg_node_custom_result'] = array('#type' => 'radios',
    '#title' => t('Enable result customization'),
    '#default_value' => variable_get('gcg_node_custom_result_' . $form['#node_type']->type, -1),
    '#options' => array(-1 => t('System default'), 0 => t('No'), 1 => t('Yes')),
  );
  $form['gcg_node']['gcg_node_input']['gcg_node_required'] = array('#type' => 'radios',
    '#title' => t('Providing an address is required'),
    '#default_value' => variable_get('gcg_node_required_' . $form['#node_type']->type, -1),
    '#options' => array(-1 => t('System default'), 0 => t('No'), 1 => t('Yes')),
  );
  $acclevels = _gcg_accuracy_levels();
  $form['gcg_node']['gcg_node_input']['gcg_node_required_accuracy'] = array('#type' => 'select',
    '#title' => t('Required accuracy'),
    '#options' => array_merge(array(-1 => t('System default')), $acclevels),
    '#default_value' => variable_get('gcg_node_required_accuracy_' . $form['#node_type']->type, -1),
    '#description' => t('%unknown means not required.', array('%unknown' => $acclevels[0])),
  );
  $form['gcg_node']['gcg_node_input']['gcg_node_privacy'] = array('#type' => 'radios',
    '#title' => t('Enable address privacy'),
    '#default_value' => variable_get('gcg_node_privacy_' . $form['#node_type']->type, -1),
    '#options' => array(-1 => t('System default'), 0 => t('No'), 1 => t('Yes')),
  );
  $form['gcg_node']['gcg_node_input']['gcg_node_required_privacy'] = array('#type' => 'select',
    '#title' => t('Required privacy'),
    '#options' => array_merge(array(-1 => t('System default')), _gcg_privacy_levels()),
    '#default_value' => variable_get('gcg_node_required_privacy_' . $form['#node_type']->type, -1),
  );
    
  if ($enabled) {
    $form['gcg_node']['display'] = array('#type' => 'fieldset',
      '#title' => t('Display settings'),
    );
    if (!variable_get('gcg_node_enable_dsp', 1)) {
      $form['gcg_node']['display']['disabled'] = array('#type' => 'item',
        '#value' => t('Map display disabled for all nodes.'),
      );
    } else {
      $enabled = variable_get('gcg_node_enable_dsp_' . $form['#node_type']->type, 1);
      if ($enabled) {
        $form['gcg_node']['display']['#collapsible'] = TRUE;
        $form['gcg_node']['display']['#collapsed'] = !$enabled;
      } else {
        $form['gcg_node']['display']['#collapsed'] = TRUE;
      }
      $form['gcg_node']['display']['gcg_node_enable_dsp'] = array('#type' => 'checkbox',
        '#title' => t('Enable map display'),
        '#default_value' => $enabled,
        '#description' => t('Uncheck this option if you would like to disable map display for this node type.'),
      );
      $opt = array('system' => t('System default'),'block' => t('In block'), 'node_left' => t('In content, floated left'), 'node_right' => t('In content, floated right'));
      if (variable_get('gcg_node_dsp_f_context', 0)) {
        $f = variable_get('gcg_node_dsp_context', 'block');
        $form['gcg_node']['display']['forced'] = array('#type' => 'item',
          '#value' => t('Forced display context: @context', array('@context' => $opt[$f])),
        );
        $form['gcg_node']['display']['gcg_node_dsp_context'] = array('#type' => 'hidden',
          '#value' => $f,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_context'] = array('#type' => 'hidden', '#value' => 1);
      } else {
        $form['gcg_node']['display']['gcg_node_dsp_context'] = array('#type' => 'radios',
          '#title' => t('Default display context'),
          '#default_value' => variable_get('gcg_node_dsp_context_' . $form['#node_type']->type, 'system'),
          '#options' => $opt,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_context'] = array('#type' => 'checkbox',
          '#title' => t('Force default display context'),
          '#default_value' => variable_get('gcg_node_dsp_f_context_' . $form['#node_type']->type, 0),
        );
      }
      $opt = array(-1 => t('System default'), 0 => t('If user requests'), 1 => t('On page load'));
      if (variable_get('gcg_node_dsp_f_map_onload', 0)) {
        $sys = variable_get('gcg_node_dsp_map_onload', 0);
        $form['gcg_node']['display']['forced_maponload'] = array('#type' => 'item',
          '#value' => t('Forced map display: @onload', array('@onload' => $opt[$sys])),
        );
        $form['gcg_node']['display']['gcg_node_dsp_map_onload'] = array('#type' => 'hidden',
          '#value' => $sys,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_map_onload'] = array('#type' => 'hidden', '#value' => 1);
      } else {
        $form['gcg_node']['display']['gcg_node_dsp_map_onload'] = array('#type' => 'radios',
          '#title' => t('Default map display'),
          '#default_value' => variable_get('gcg_node_dsp_map_onload_' . $form['#node_type']->type, -1),
          '#options' => $opt,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_map_onload'] = array('#type' => 'checkbox',
          '#title' => t('Force default map display'),
          '#default_value' => variable_get('gcg_node_dsp_f_map_onload_' . $form['#node_type']->type, 0),
        );
      }
      $opt = array('system' => t('System default'), 'static' => t('Static'), 'dynamic' => t('Dynamic'));
      if (variable_get('gcg_node_dsp_f_mapapi_type', 0)) {
        $sys = variable_get('gcg_node_dsp_mapapi_type', 'static');
        $form['gcg_node']['display']['forced_mapapitype'] = array('#type' => 'item',
          '#value' => t('Forced map API type: @type', array('@type' => $opt[$sys])),
        );
        $form['gcg_node']['display']['gcg_node_dsp_mapapi_type'] = array('#type' => 'hidden',
          '#value' => $sys,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_mapapi_type'] = array('#type' => 'hidden', '#value' => 1);
      } else {
        $form['gcg_node']['display']['gcg_node_dsp_mapapi_type'] = array('#type' => 'radios',
          '#title' => t('Default map API type'),
          '#default_value' => variable_get('gcg_node_dsp_mapapi_type_' . $form['#node_type']->type, 'system'),
          '#options' => $opt,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_mapapi_type'] = array('#type' => 'checkbox',
          '#title' => t('Force default map API type'),
          '#default_value' => variable_get('gcg_node_dsp_f_mapapi_type_' . $form['#node_type']->type, 0),
        );
      }
      if (variable_get('gcg_node_dsp_f_map_height', 0)) {
        $sys = variable_get('gcg_node_dsp_map_height', 0);
        $systxt = $sys == 0 ? 200 : $sys;
        $form['gcg_node']['display']['forced_mapheight'] = array('#type' => 'item',
          '#value' => t('Forced map height: @heightpx', array('@height' => $systxt)),
        );
        $form['gcg_node']['display']['gcg_node_dsp_map_height'] = array('#type' => 'hidden',
          '#value' => $sys,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_map_height'] = array('#type' => 'hidden', '#value' => 1);
      } else {
        $form['gcg_node']['display']['gcg_node_dsp_map_height'] = array('#type' => 'textfield',
          '#title' => t('Default map height'),
          '#default_value' => variable_get('gcg_node_dsp_map_height_' . $form['#node_type']->type, -1),
          '#size' => 6,
          '#maxlength' => 4,
          '#field_suffix' => 'px',
          '#description' => t('-1 means system default and 0 means theme default.'),
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_map_height'] = array('#type' => 'checkbox',
          '#title' => t('Force default map height'),
          '#default_value' => variable_get('gcg_node_dsp_f_map_height_' . $form['#node_type']->type, 0),
        );
      }
      $opt = array('system' => t('System default'), 'normal' => t('Normal'), 'satellite' => t('Satellite'), 'hybrid' => t('Hybrid'), 'physical' => t('Physical'));
      if (variable_get('gcg_node_dsp_f_map_type', 0)) {
        $sys = variable_get('gcg_node_dsp_map_type', 'normal');
        $form['gcg_node']['display']['forced_maptype'] = array('#type' => 'item',
          '#value' => t('Forced map type: @type', array('@type' => $opt[$sys])),
        );
        $form['gcg_node']['display']['gcg_node_dsp_map_type'] = array('#type' => 'hidden',
          '#value' => $sys,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_map_type'] = array('#type' => 'hidden', '#value' => 1);
      } else {
        $form['gcg_node']['display']['gcg_node_dsp_map_type'] = array('#type' => 'radios',
          '#title' => t('Default map type'),
          '#default_value' => variable_get('gcg_node_dsp_map_type_' . $form['#node_type']->type, 'system'),
          '#options' => $opt,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_map_type'] = array('#type' => 'checkbox',
          '#title' => t('Force default map type'),
          '#default_value' => variable_get('gcg_node_dsp_f_map_type_' . $form['#node_type']->type, 0),
        );
      }
      $opt = array('system' => t('System default'), 'roadmap' => t('Roadmap'), 'mobile' => t('Mobile'));
      if (variable_get('gcg_node_dsp_f_smap_type', 0)) {
        $sys = variable_get('gcg_node_dsp_smap_type', 'roadmap');
        $form['gcg_node']['display']['forced_smaptype'] = array('#type' => 'item',
          '#value' => t('Forced static map type: @type', array('@type' => $opt[$sys])),
        );
        $form['gcg_node']['display']['gcg_node_dsp_smap_type'] = array('#type' => 'hidden',
          '#value' => $sys,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_smap_type'] = array('#type' => 'hidden', '#value' => 1);
      } else {
        $form['gcg_node']['display']['gcg_node_dsp_smap_type'] = array('#type' => 'radios',
          '#title' => t('Default static map type'),
          '#default_value' => variable_get('gcg_node_dsp_smap_type_' . $form['#node_type']->type, 'system'),
          '#options' => $opt,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_smap_type'] = array('#type' => 'checkbox',
          '#title' => t('Force default static map type'),
          '#default_value' => variable_get('gcg_node_dsp_f_smap_type_' . $form['#node_type']->type, 0),
        );
      }
      $opt = array(-1 => t('System default'), 0 => t('Disabled'), 1 => t('Enabled'));
      if (variable_get('gcg_node_dsp_f_map_drag', 0)) {
        $sys = variable_get('gcg_node_dsp_map_drag', 1);
        $form['gcg_node']['display']['forced_mapdrag'] = array('#type' => 'item',
          '#value' => t('Forced map dragging state: @dragging', array('@dragging' => $opt[$sys])),
        );
        $form['gcg_node']['display']['gcg_node_dsp_map_drag'] = array('#type' => 'hidden',
          '#value' => $sys,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_map_drag'] = array('#type' => 'hidden', '#value' => 1);
      } else {
        $form['gcg_node']['display']['gcg_node_dsp_map_drag'] = array('#type' => 'radios',
          '#title' => t('Enable map dragging'),
          '#default_value' => variable_get('gcg_node_dsp_map_drag_' . $form['#node_type']->type, -1),
          '#options' => $opt,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_map_drag'] = array('#type' => 'checkbox',
          '#title' => t('Force map dragging'),
          '#default_value' => variable_get('gcg_node_dsp_f_map_drag_' . $form['#node_type']->type, 0),
        );
      }
      $opt = array(-1 => t('System default'), 0 => t('Disabled'), 1 => t('Enabled'));
      if (variable_get('gcg_node_dsp_f_map_zoom', 0)) {
        $sys = variable_get('gcg_node_dsp_map_zoom', 0);
        $form['gcg_node']['display']['forced_mapzoom'] = array('#type' => 'item',
          '#value' => t('Forced map zoom state: @zoom', array('@zoom' => $opt[$sys])),
        );
        $form['gcg_node']['display']['gcg_node_dsp_map_zoom'] = array('#type' => 'hidden',
          '#value' => $sys,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_map_zoom'] = array('#type' => 'hidden', '#value' => 1);
      } else {
        $form['gcg_node']['display']['gcg_node_dsp_map_zoom'] = array('#type' => 'radios',
          '#title' => t('Enable map zoom'),
          '#default_value' => variable_get('gcg_node_dsp_map_zoom_' . $form['#node_type']->type, -1),
          '#options' => $opt,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_map_zoom'] = array('#type' => 'checkbox',
          '#title' => t('Force map zoom'),
          '#default_value' => variable_get('gcg_node_dsp_f_map_zoom_' . $form['#node_type']->type, 0),
        );
      }
      $opt = array(-1 => t('System default'), 0 => t('Disabled'), 1 => t('Enabled'));
      if (variable_get('gcg_node_dsp_f_map_scontrol', 0)) {
        $sys = variable_get('gcg_node_dsp_map_scontrol', 0);
        $form['gcg_node']['display']['forced_mapscontrol'] = array('#type' => 'item',
          '#value' => t('Forced small map control state: @scontrol', array('@scontrol' => $opt[$sys])),
        );
        $form['gcg_node']['display']['gcg_node_dsp_map_scontrol'] = array('#type' => 'hidden',
          '#value' => $sys,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_map_scontrol'] = array('#type' => 'hidden', '#value' => 1);
      } else {
        $form['gcg_node']['display']['gcg_node_dsp_map_scontrol'] = array('#type' => 'radios',
          '#title' => t('Enable small map control'),
          '#default_value' => variable_get('gcg_node_dsp_map_scontrol_' . $form['#node_type']->type, -1),
          '#options' => $opt,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_map_scontrol'] = array('#type' => 'checkbox',
          '#title' => t('Force small map control'),
          '#default_value' => variable_get('gcg_node_dsp_f_map_scontrol_' . $form['#node_type']->type, 0),
        );
      }
      if (variable_get('gcg_node_dsp_f_weight', 0)) {
        $sys = variable_get('gcg_node_dsp_weight', 0);
        $form['gcg_node']['display']['forced_weight'] = array('#type' => 'item',
          '#value' => t('Forced display weight: @weight', array('@weight' => $sys)),
        );
        $form['gcg_node']['display']['gcg_node_dsp_weight'] = array('#type' => 'hidden',
          '#value' => $sys,
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_weight'] = array('#type' => 'hidden', '#value' => 1);
      } else {
        $form['gcg_node']['display']['gcg_node_dsp_weight'] = array('#type' => 'weight',
          '#title' => t('Default weight'),
          '#default_value' => variable_get('gcg_node_dsp_weight_' . $form['#node_type']->type, 0),
          '#delta' => 20,
          '#description' => t('Applied only if context is node.'),
        );
        $form['gcg_node']['display']['gcg_node_dsp_f_weight'] = array('#type' => 'checkbox',
          '#title' => t('Force default weight'),
          '#default_value' => variable_get('gcg_node_dsp_f_weight_' . $form['#node_type']->type, 0),
        );
      }
    }
  }
}