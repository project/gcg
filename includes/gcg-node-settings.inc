<?php

function _gcg_node_settings_form() {
  //can be replaced by a super-array
  $form['input'] = array('#type' => 'fieldset',
    '#title' => t('Site wide input settings'),
  );
  $form['input']['gcg_node_result_format'] = array('#type' => 'radios',
    '#title' => t('Default result display format'),
    '#default_value' => variable_get('gcg_node_result_format', 'full'),
    '#options' => array('compact' => t('Compact - accuracy, address and coordinates'), 'full' => t('Full - all available fields')),
  );
  $form['input']['gcg_node_result_on_map'] = array('#type' => 'checkbox',
    '#title' => t('Display result on map'),
    '#default_value' => variable_get('gcg_node_result_on_map', 1),
  );
  //TODO: adding 0 as theme default.
  $form['input']['gcg_node_result_map_height'] = array('#type' => 'textfield',
    '#title' => t('Height of the result map if displayed'),
    '#default_value' => variable_get('gcg_node_result_map_height', 200),
    '#size' => 5,
    '#maxlength' => 3,
    '#field_suffix' => 'px',
  );
  $list[] = t('In case of enabled result fields, the item will always return "custom" value.');
  $list[] = t('In case of disabled result fields and disabled customization, the item will always return "non-custom" value. In this case only geocoder returned addresses can be submitted.');
  $list[] = t('In case of enabled customization, the user allowed to submit completely custom, modified and "non-custom", geocoder returned values.');
  $form['input']['gcg_result_description'] = array('#type' => 'item',
    '#title' => t('Result editing behavior'),
    '#value' => theme('item_list', $list) . t('<strong>NOTE</strong>: GCG Taxonomy and possibly other modules take difference between custom and non-custom values.'),
  );
  $form['input']['gcg_node_disabled'] = array('#type' => 'checkbox',
    '#title' => t('Disable result fields'),
    '#default_value' => variable_get('gcg_node_disabled', TRUE),
  );
  $form['input']['gcg_node_custom_result'] = array('#type' => 'checkbox',
    '#title' => t('Enable result customization'),
    '#default_value' => variable_get('gcg_node_custom_result', 1),
  );
  $form['input']['gcg_node_required'] = array('#type' => 'checkbox',
    '#title' => t('Providing an address is required'),
    '#default_value' => variable_get('gcg_node_required', 0),
  );

  $acclevels = _gcg_accuracy_levels();
  $form['input']['gcg_node_required_accuracy'] = array('#type' => 'select',
    '#title' => t('Required accuracy'),
    '#options' => $acclevels,
    '#default_value' => variable_get('gcg_node_required_accuracy', 0),
    '#description' => t('%unknown means not required.', array('%unknown' => $acclevels[0])),
  );
  $form['input']['gcg_node_privacy'] = array('#type' => 'checkbox',
    '#title' => t('Enable address privacy'),
    '#default_value' => variable_get('gcg_node_privacy', 0),
  );
  $form['input']['gcg_node_required_privacy'] = array('#type' => 'select',
    '#title' => t('Required privacy'),
    '#options' => _gcg_privacy_levels(),
    '#default_value' => variable_get('gcg_node_required_privacy', 0),
  );

  $form['display'] = array('#type' => 'fieldset',
    '#title' => t('Site wide display settings'),
  );
  $form['display']['gcg_node_enable_dsp'] = array('#type' => 'checkbox',
    '#title' => t('Enable map display'),
    '#default_value' => variable_get('gcg_node_enable_dsp', 1),
    '#description' => t('Uncheck this option if you would like to disable map display for all nodes.'),
  );
  $form['display']['gcg_node_dsp_context'] = array('#type' => 'radios',
    '#title' => t('Default display context'),
    '#default_value' => variable_get('gcg_node_dsp_context', 'block'),
    '#options' => array('block' => t('In block'), 'node_left' => t('In content, floated left'), 'node_right' => t('In content, floated right')),
  );
  $form['display']['gcg_node_dsp_f_context'] = array('#type' => 'checkbox',
    '#title' => t('Force default display context'),
    '#default_value' => variable_get('gcg_node_dsp_f_context', 0),
  );
  $form['display']['gcg_node_dsp_map_onload'] = array('#type' => 'radios',
    '#title' => t('Default map display'),
    '#default_value' => variable_get('gcg_node_dsp_map_onload', 0),
    '#options' => array(t('If user requests'), t('On page load')),
  );
  $form['display']['gcg_node_dsp_f_map_onload'] = array('#type' => 'checkbox',
    '#title' => t('Force default map display'),
    '#default_value' => variable_get('gcg_node_dsp_f_map_onload', 0),
  );
  $form['display']['gcg_node_dsp_map_height'] = array('#type' => 'textfield',
    '#title' => t('Default map height'),
    '#default_value' => variable_get('gcg_node_dsp_map_height', 0),
    '#size' => 6,
    '#maxlength' => 4,
    '#field_suffix' => 'px',
    '#description' => t('0 means theme default.'),
  );
  $form['display']['gcg_node_dsp_f_map_height'] = array('#type' => 'checkbox',
    '#title' => t('Force default map height'),
    '#default_value' => variable_get('gcg_node_dsp_f_map_height', 0),
  );
  $form['display']['gcg_node_dsp_mapapi_type'] = array('#type' => 'radios',
    '#title' => t('Default map API type'),
    '#default_value' => variable_get('gcg_node_dsp_mapapi_type', 'static'),
    '#options' => array('static' => t('Static'), 'dynamic' => t('Dynamic')),
  );
  $form['display']['gcg_node_dsp_f_mapapi_type'] = array('#type' => 'checkbox',
    '#title' => t('Force default map API type'),
    '#default_value' => variable_get('gcg_node_dsp_f_mapapi_type', 0),
  );
  $form['display']['gcg_node_dsp_map_type'] = array('#type' => 'radios',
    '#title' => t('Default map type'),
    '#default_value' => variable_get('gcg_node_dsp_map_type', 'normal'),
    '#options' => array('normal' => t('Normal'), 'satellite' => t('Satellite'), 'hybrid' => t('Hybrid'), 'physical' => t('Physical')),
  );
  $form['display']['gcg_node_dsp_f_map_type'] = array('#type' => 'checkbox',
    '#title' => t('Force default map type'),
    '#default_value' => variable_get('gcg_node_dsp_f_map_type', 0),
  );
  $form['display']['gcg_node_dsp_smap_type'] = array('#type' => 'radios',
    '#title' => t('Default static map type'),
    '#default_value' => variable_get('gcg_node_dsp_smap_type', 'roadmap'),
    '#options' => array('roadmap' => t('Roadmap'), 'mobile' => t('Mobile')),
  );
  $form['display']['gcg_node_dsp_f_smap_type'] = array('#type' => 'checkbox',
    '#title' => t('Force default static map type'),
    '#default_value' => variable_get('gcg_node_dsp_f_smap_type', 0),
  );
  $form['display']['gcg_node_dsp_map_drag'] = array('#type' => 'checkbox',
    '#title' => t('Enable map dragging'),
    '#default_value' => variable_get('gcg_node_dsp_map_drag', 1),
  );
  $form['display']['gcg_node_dsp_f_map_drag'] = array('#type' => 'checkbox',
    '#title' => t('Force map dragging'),
    '#default_value' => variable_get('gcg_node_dsp_f_map_drag', 0),
  );
  $form['display']['gcg_node_dsp_map_zoom'] = array('#type' => 'checkbox',
    '#title' => t('Enable map zoom'),
    '#default_value' => variable_get('gcg_node_dsp_map_zoom', 1),
  );
  $form['display']['gcg_node_dsp_f_map_zoom'] = array('#type' => 'checkbox',
    '#title' => t('Force map zoom'),
    '#default_value' => variable_get('gcg_node_dsp_f_map_zoom', 0),
  );
  $form['display']['gcg_node_dsp_map_scontrol'] = array('#type' => 'checkbox',
    '#title' => t('Enable small map control'),
    '#default_value' => variable_get('gcg_node_dsp_map_scontrol', 1),
  );
  $form['display']['gcg_node_dsp_f_map_scontrol'] = array('#type' => 'checkbox',
    '#title' => t('Force small map control'),
    '#default_value' => variable_get('gcg_node_dsp_f_map_scontrol', 0),
  );
  $form['display']['gcg_node_dsp_weight'] = array('#type' => 'weight',
    '#title' => t('Default weight'),
    '#default_value' => variable_get('gcg_node_dsp_weight', 0),
    '#delta' => 20,
    '#description' => t('Applied only if context is node.'),
  );
  $form['display']['gcg_node_dsp_f_weight'] = array('#type' => 'checkbox',
    '#title' => t('Force default weight'),
    '#default_value' => variable_get('gcg_node_dsp_f_map_height', 0),
  );

  return system_settings_form($form);
}