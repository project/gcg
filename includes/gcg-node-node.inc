<?php

function _gcg_node_node_form(&$form) {
  $node = $form['#node'];
  if (variable_get('gcg_node_type_enabled_' . $node->type, 0)) {
    $display_format = variable_get('gcg_node_result_format_' . $node->type, 'system');
    $display_map = variable_get('gcg_node_result_on_map_' . $node->type, -1);
    $map_height = variable_get('gcg_node_result_map_height_' . $node->type, -1);
    $disabled = variable_get('gcg_node_disabled_' . $node->type, -1);
    $cr = variable_get('gcg_node_custom_result_' . $node->type, -1);
    $required = variable_get('gcg_node_required_' . $node->type, -1);
    $ra = variable_get('gcg_node_required_accuracy_' . $node->type, -1);
    $privacy = variable_get('gcg_node_privacy_' . $node->type, -1);
    $rp = variable_get('gcg_node_required_privacy_' . $node->type, -1);
    $form['gcg_node'] = array('#type' => 'fieldset',
      '#title' => t('Node address settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    if (!empty($node->nid) && !gcg_empty($node->gcg_node_address)) {
      $form['gcg_node']['gcg_node_address_delete'] = array('#type' => 'checkbox',
        '#title' => t('Delete node address'),
        '#default_value' => 0,
      );
    }
    $form['gcg_node']['gcg_node_address'] = array(
      '#type' => 'gcg',
      '#title' => t('Node address'),
      '#default_value' => $node->gcg_node_address,
      '#gcg_result_format' => $display_format == 'system' ? variable_get('gcg_node_result_format', 'full') : $display_format,
      '#gcg_result_on_map' => $display_map == -1 ? variable_get('gcg_node_result_on_map', 1) : $display_map,
      '#gcg_result_map_height' => $map_height == -1 ? variable_get('gcg_node_result_map_height', 200) : $map_height,
      '#disabled' => $disabled == -1 ? variable_get('gcg_node_disabled', 1) : $disabled,
      '#gcg_custom_result' => $cr == -1 ? variable_get('gcg_node_custom_result', 1) : $cr,
      '#required' => $required == -1 ? variable_get('gcg_node_requred', 0) : $required,
      '#gcg_required_accuracy' => $ra == -1 ? variable_get('gcg_node_required_accuracy', 0) : $ra,
      '#gcg_privacy' => $privacy == -1 ? variable_get('gcg_node_privacy', 0) : $privacy,
      '#gcg_required_privacy' => $rp == -1 ? variable_get('gcg_node_required_privacy', 0) : $rp,
    );

    if (!user_access('administer gcg node') && !user_access('administer gcg node display')) {
      return;
    }
    $form['gcg_node_display'] = array('#type' => 'fieldset',
      '#title' => t('Node map display settings'),
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    if (variable_get('gcg_node_enable_dsp', 1)) {
      if (variable_get('gcg_node_enable_dsp_' . $node->type, 1)) {
        $dsp = &$node->gcg_node_display;
        $dsp = !empty($dsp) ? $dsp : array();
        $form['gcg_node_display']['enable'] = array('#type' => 'checkbox',
          '#title' => t('Enable map display'),
          '#default_value' => isset($dsp['enable']) ? $dsp['enable'] : 1,
          '#description' => t('Uncheck this option if you would like to disable map display for this node.'),
        );
        
        //context
        $opt = array('system' => t('System default'),'block' => t('In block'), 'node_left' => t('In content, floated left'), 'node_right' => t('In content, floated right'));
        $sys = variable_get('gcg_node_dsp_context', 'block');
        if (variable_get('gcg_node_dsp_f_context', 0)) {
          $form['gcg_node_display']['forced_context'] = array('#type' => 'item',
            '#value' => t('Forced display context: @context', array('@context' => $opt[$sys])),
          );
          $form['gcg_node_display']['context'] = array('#type' => 'hidden',
            '#value' => $sys,
          );
        } else if (variable_get('gcg_node_dsp_f_context_' . $node->type, 0)) {
          $type = variable_get('gcg_node_dsp_context_' . $node->type, 'system');
          $type = $type == 'system' ? $sys : $type;
          $form['gcg_node_display']['forced_context'] = array('#type' => 'item',
            '#value' => t('Forced display context: @context', array('@context' => $opt[$type])),
          );
          $form['gcg_node_display']['context'] = array('#type' => 'hidden',
            '#value' => $type,
          );
        } else {
          $form['gcg_node_display']['context'] = array('#type' => 'radios',
            '#title' => t('Default display context'),
            '#default_value' => isset($dsp['context']) ? $dsp['context'] : 'system',
            '#options' => $opt,
          );
        }

        //onload
        $opt = array(-1 => t('System default'), 0 => t('If user requests'), 1 => t('On page load'));
        $sys = variable_get('gcg_node_dsp_map_onload', 0);
        if (variable_get('gcg_node_dsp_f_map_onload', 0)) {
          $form['gcg_node_display']['forced_maponload'] = array('#type' => 'item',
            '#value' => t('Forced map display: @onload', array('@onload' => $opt[$sys])),
          );
          $form['gcg_node_display']['map_onload'] = array('#type' => 'hidden',
            '#value' => $sys,
          );
        } else if (variable_get('gcg_node_dsp_f_map_onload_' . $node->type, 0)) {
          $type = variable_get('gcg_node_dsp_map_onload_' . $node->type, -1);
          $type = $type == -1 ? $sys : $type;
          $form['gcg_node_display']['forced_maponload'] = array('#type' => 'item',
            '#value' => t('Forced map display: @onload', array('@onload' => $opt[$type])),
          );
          $form['gcg_node_display']['map_onload'] = array('#type' => 'hidden',
            '#value' => $type,
          );
        } else {
          $form['gcg_node_display']['map_onload'] = array('#type' => 'radios',
            '#title' => t('Map display'),
            '#default_value' => isset($dsp['map_onload']) ? $dsp['map_onload'] : -1,
            '#options' => $opt,
          );
        }

        //mapapitype
        $opt = array('system' => t('System default'), 'static' => t('Static'), 'dynamic' => t('Dynamic'));
        $sys = variable_get('gcg_node_dsp_mapapi_type', 'static');
        if (variable_get('gcg_node_dsp_f_mapapi_type', 0)) {
          $form['gcg_node_display']['forced_mapapitype'] = array('#type' => 'item',
            '#value' => t('Forced map API type: @type', array('@type' => $opt[$sys])),
          );
          $form['gcg_node_display']['mapapi_type'] = array('#type' => 'hidden',
            '#value' => $sys,
          );
        } else if (variable_get('gcg_node_dsp_f_mapapi_type_' . $node->type, 0)) {
          $type = variable_get('gcg_node_dsp_mapapi_type_' . $node->type, 'system');
          $type = $type == 'system' ? $sys : $type;
          $form['gcg_node_display']['forced_mapapitype'] = array('#type' => 'item',
            '#value' => t('Forced map API type: @type', array('@type' => $opt[$type])),
          );
          $form['gcg_node_display']['mapapi_type'] = array('#type' => 'hidden',
            '#value' => $type,
          );
        } else {
          $form['gcg_node_display']['mapapi_type'] = array('#type' => 'radios',
            '#title' => t('Map API type'),
            '#default_value' => isset($dsp['mapapi_type']) ? $dsp['mapapi_type'] : 'system',
            '#options' => $opt,
          );
        }

        //mapheight
        $sys = variable_get('gcg_node_dsp_map_height', 0);
        if (variable_get('gcg_node_dsp_f_map_height', 0)) {
          $systxt = $sys == 0 ? 200 : $sys;
          $form['gcg_node_display']['forced_mapheight'] = array('#type' => 'item',
            '#value' => t('Forced map height: @heightpx', array('@height' => $systxt)),
          );
          $form['gcg_node_display']['map_height'] = array('#type' => 'hidden',
            '#value' => $sys,
          );
        } else if (variable_get('gcg_node_dsp_f_map_height_' . $node->type, 0)) {
          $type = variable_get('gcg_node_dsp_map_height_' . $node->type, -1);
          $type = $type == -1 ? $sys : $type;
          $typetxt = $type == 0 ? 200 : $type;
          $form['gcg_node_display']['forced_mapheight'] = array('#type' => 'item',
            '#value' => t('Forced map height: @heightpx', array('@height' => $typetxt)),
          );
          $form['gcg_node_display']['map_height'] = array('#type' => 'hidden',
            '#value' => $type,
          );
        } else {
          $form['gcg_node_display']['map_height'] = array('#type' => 'textfield',
            '#title' => t('Map height'),
            '#default_value' => isset($dsp['map_height']) ? $dsp['map_height'] : -1,
            '#size' => 6,
            '#maxlength' => 4,
            '#field_suffix' => 'px',
            '#description' => t('-1 means system default and 0 mean theme default.'),
          );
        }

        //maptype
        $opt = array('system' => t('System default'), 'normal' => t('Normal'), 'satellite' => t('Satellite'), 'hybrid' => t('Hybrid'), 'physical' => t('Physical'));
        $sys = variable_get('gcg_node_dsp_map_type', 'normal');
        if (variable_get('gcg_node_dsp_f_map_type', 0)) {
          $form['gcg_node_display']['forced_maptype'] = array('#type' => 'item',
            '#value' => t('Forced map type: @type', array('@type' => $opt[$sys])),
          );
          $form['gcg_node_display']['map_type'] = array('#type' => 'hidden',
            '#value' => $sys,
          );
        } else if (variable_get('gcg_node_dsp_f_map_type_' . $node->type, 0)) {
          $type = variable_get('gcg_node_dsp_map_type_' . $node->type, 'system');
          $type = $type == 'system' ? $sys : $type;
          $form['gcg_node_display']['forced_maptype'] = array('#type' => 'item',
            '#value' => t('Forced map type: @type', array('@type' => $opt[$type])),
          );
          $form['gcg_node_display']['map_type'] = array('#type' => 'hidden',
            '#value' => $type,
          );
        } else {
          $form['gcg_node_display']['map_type'] = array('#type' => 'radios',
            '#title' => t('Map type'),
            '#default_value' => isset($dsp['map_type']) ? $dsp['map_type'] : 'system',
            '#options' => $opt,
          );
        }

        //smaptype
        $opt = array('system' => t('System default'), 'roadmap' => t('Roadmap'), 'mobile' => t('Mobile'));
        $sys = variable_get('gcg_node_dsp_smap_type', 'roadmap');
        if (variable_get('gcg_node_dsp_f_smap_type', 0)) {
          $form['gcg_node_display']['forced_smaptype'] = array('#type' => 'item',
            '#value' => t('Forced static map type: @type', array('@type' => $opt[$sys])),
          );
          $form['gcg_node_display']['smap_type'] = array('#type' => 'hidden',
            '#value' => $sys,
          );
        } else if (variable_get('gcg_node_dsp_f_smap_type_' . $node->type, 0)) {
          $type = variable_get('gcg_node_dsp_smap_type_' . $node->type, 'system');
          $type = $type == 'system' ? $sys : $type;
          $form['gcg_node_display']['forced_smaptype'] = array('#type' => 'item',
            '#value' => t('Forced static map type: @type', array('@type' => $opt[$type])),
          );
          $form['gcg_node_display']['smap_type'] = array('#type' => 'hidden',
            '#value' => $type,
          );
        } else {
          $form['gcg_node_display']['smap_type'] = array('#type' => 'radios',
            '#title' => t('Static map type'),
            '#default_value' => isset($dsp['smap_type']) ? $dsp['smap_type'] : 'system',
            '#options' => $opt,
          );
        }

        //mapdrag
        $opt = array(-1 => t('System default'), 0 => t('Disabled'), 1 => t('Enabled'));
        $sys = variable_get('gcg_node_dsp_map_drag', 1);
        if (variable_get('gcg_node_dsp_f_map_drag', 0)) {
          $form['gcg_node_display']['forced_mapdrag'] = array('#type' => 'item',
            '#value' => t('Forced map dragging state: @dragging', array('@dragging' => $opt[$sys])),
          );
          $form['gcg_node_display']['map_drag'] = array('#type' => 'hidden',
            '#value' => $sys,
          );
        } else if (variable_get('gcg_node_dsp_f_map_drag_' . $node->type, 0)) {
          $type = variable_get('gcg_node_dsp_map_drag_' . $node->type, -1);
          $type = $type == -1 ? $sys : $type;
          $form['gcg_node_display']['forced_mapdrag'] = array('#type' => 'item',
            '#value' => t('Forced map dragging state: @dragging', array('@dragging' => $opt[$type])),
          );
          $form['gcg_node_display']['map_drag'] = array('#type' => 'hidden',
            '#value' => $type,
          );
        } else {
          $form['gcg_node_display']['map_drag'] = array('#type' => 'radios',
            '#title' => t('Enable map dragging'),
            '#default_value' => isset($dsp['map_drag']) ? $dsp['map_drag'] : -1,
            '#options' => $opt,
          );
        }

        //mapzoom
        $opt = array(-1 => t('System default'), 0 => t('Disabled'), 1 => t('Enabled'));
        $sys = variable_get('gcg_node_dsp_map_zoom', 0);
        if (variable_get('gcg_node_dsp_f_map_zoom', 0)) {
          $form['gcg_node_display']['forced_mapzoom'] = array('#type' => 'item',
            '#value' => t('Forced map zoom state: @zoom', array('@zoom' => $opt[$sys])),
          );
          $form['gcg_node_display']['map_zoom'] = array('#type' => 'hidden',
            '#value' => $sys,
          );
        } else if (variable_get('gcg_node_dsp_f_map_zoom_' . $node->type, 0)) {
          $type = variable_get('gcg_node_dsp_map_zoom_' . $node->type, -1);
          $type = $type == -1 ? $sys : $type;
          $form['gcg_node_display']['forced_mapzoom'] = array('#type' => 'item',
            '#value' => t('Forced map zoom state: @zoom', array('@zoom' => $opt[$type])),
          );
          $form['gcg_node_display']['map_zoom'] = array('#type' => 'hidden',
            '#value' => $type,
          );
        } else {
          $form['gcg_node_display']['map_zoom'] = array('#type' => 'radios',
            '#title' => t('Enable map zoom'),
            '#default_value' => isset($dsp['map_zoom']) ? $dsp['map_zoom'] : -1,
            '#options' => $opt,
          );
        }

        //small control
        $opt = array(-1 => t('System default'), 0 => t('Disabled'), 1 => t('Enabled'));
        $sys = variable_get('gcg_node_dsp_map_scontrol', 0);
        if (variable_get('gcg_node_dsp_f_map_scontrol', 0)) {
          $form['gcg_node_display']['forced_mapscontrol'] = array('#type' => 'item',
            '#value' => t('Forced small map control state: @scontrol', array('@scontrol' => $opt[$sys])),
          );
          $form['gcg_node_display']['map_scontrol'] = array('#type' => 'hidden',
            '#value' => $sys,
          );
        } else if (variable_get('gcg_node_dsp_f_map_scontrol_' . $node->type, 0)) {
          $type = variable_get('gcg_node_dsp_map_scontrol_' . $node->type, -1);
          $type = $type == -1 ? $sys : $type;
          $form['gcg_node_display']['forced_mapscontrol'] = array('#type' => 'item',
            '#value' => t('Forced small map control state: @scontrol', array('@scontrol' => $opt[$type])),
          );
          $form['gcg_node_display']['map_scontrol'] = array('#type' => 'hidden',
            '#value' => $type,
          );
        } else {
          $form['gcg_node_display']['map_scontrol'] = array('#type' => 'radios',
            '#title' => t('Enable small map control'),
            '#default_value' => isset($dsp['map_scontrol']) ? $dsp['map_scontrol'] : -1,
            '#options' => $opt,
          );
        }

        //weight
        $sys = variable_get('gcg_node_dsp_weight', 0);
        if (variable_get('gcg_node_dsp_f_weight', 0)) {
          $form['gcg_node_display']['forced_weight'] = array('#type' => 'item',
            '#value' => t('Forced display weight: @weight', array('@weight' => $sys)),
          );
          $form['gcg_node_display']['weight'] = array('#type' => 'hidden',
            '#value' => $sys,
          );
        } else if (variable_get('gcg_node_dsp_f_weight_' . $node->type, 0)) {
          $type = variable_get('gcg_node_dsp_weight_' . $node->type, 0);
          $form['gcg_node_display']['forced_weight'] = array('#type' => 'item',
            '#value' => t('Forced display weight: @weight', array('@weight' => $type)),
          );
          $form['gcg_node_display']['weight'] = array('#type' => 'hidden',
            '#value' => $type,
          );
        } else {
          $form['gcg_node_display']['weight'] = array('#type' => 'weight',
            '#title' => t('Display weight'),
            '#default_value' => isset($dsp['weight']) ? $dsp['weight'] : 0,
            '#delta' => 20,
            '#description' => t('Applied only if context is node.'),
          );
        }
      } else {
        $form['gcg_node_display']['disabled'] = array('#type' => 'item',
          '#value' => t('Map display disabled for this node type.'),
        );
      }
    } else {
      $form['gcg_node_display']['disabled'] = array('#type' => 'item',
        '#value' => t('Map display disabled for all nodes.'),
      );
    }
  }
}